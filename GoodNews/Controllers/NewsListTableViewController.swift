//
//  NewsListTableViewController.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 04/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation
import UIKit

class NewsListTableViewController: UITableViewController {
    private var spotlightListVM: SpotlightListViewModel!
    private var productsListVM: ProductsListViewModel!
    private var cashListVM: CashListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let appearance = self.navigationController?.navigationBar.standardAppearance
        appearance?.backgroundColor = .purple
        appearance?.titleTextAttributes = [.foregroundColor: UIColor.white,
                .font : UIFont.init(name: "AvenirNext-DemiBold", size: 28.0)!]
        appearance?.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        
        //Consome a API e carrega os dados para a view
        let url = URL(string: "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products")!
    
        
        Webservice().getArticles(url: url) { spotlight in
            if let spotlights = spotlight {
                self.spotlightListVM = SpotlightListViewModel(spotlights: spotlights )
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
        Webservice().getCash(url: url) { cash in
            if let cash = cash {
                self.cashListVM = CashListViewModel(cash: cash)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
        Webservice().getProducts(url: url) { products in
            if let products = products {
                self.productsListVM = ProductsListViewModel(products: products)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.spotlightListVM == nil ? 0: self.spotlightListVM.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.spotlightListVM.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell", for: indexPath) as? ArticleTableViewCell else {
            fatalError("ArticleTableViewCell not found")
        }
        let spotlightVM = self.spotlightListVM.spotlightAtIndex(indexPath.row)
        let cashVM = self.cashListVM?.cashAtIndex(indexPath.row)
        let productsVM = self.productsListVM.productsAtIndex(0)
        let productsVM1 = self.productsListVM.productsAtIndex(1)
        let productsVM2 = self.productsListVM.productsAtIndex(2)
        
        //Carga dos spotlights
        let url = URL(string: spotlightVM.bannerURL)!
        let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        cell.bannerURL.image = UIImage(data: data!)

        
        //Carrega dados do digio Cash
        cell.digioTitle.text = cashVM?.title
        //Carregar imagem
        let urlCash = URL(string: cashVM?.bannerURL ?? "")!
        let dataCash = try? Data(contentsOf: urlCash) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        cell.digioImageURL.image = UIImage(data: dataCash!)
        
        //Carregar 3 imagens dos produtos
        let urlProducts = URL(string: productsVM.imageURL)!
        let dataProducts = try? Data(contentsOf: urlProducts) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        cell.imageURL.image = UIImage(data: dataProducts!)
        
        let urlProducts1 = URL(string: productsVM1.imageURL)!
        let dataProducts1 = try? Data(contentsOf: urlProducts1) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        cell.imageURL1.image = UIImage(data: dataProducts1!)
        
        let urlProducts2 = URL(string: productsVM2.imageURL)!
        let dataProducts2 = try? Data(contentsOf: urlProducts2) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        if dataProducts2 != nil {
            cell.imageURL2.image = UIImage(data: dataProducts2!)
        }
        
        return cell
    }
    
}
