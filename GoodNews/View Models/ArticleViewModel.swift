//
//  ArticleViewModel.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 06/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

struct SpotlightListViewModel {
    let spotlights: [Spotlight]
}

extension SpotlightListViewModel {
    var numberOfSections: Int {
        return 1
    }
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.spotlights.count
    }
    func spotlightAtIndex(_ index: Int) -> SpotlightViewModel {
        let spotlight = self.spotlights[index]
        return SpotlightViewModel(spotlight)
    }
}

struct SpotlightViewModel {
    private let spotlight: Spotlight
}

extension SpotlightViewModel {
    init(_ spotlight: Spotlight) {
        self.spotlight = spotlight
    }
}

extension SpotlightViewModel {
    
    var name: String {
        return self.spotlight.name
    
    }
    
    var bannerURL: String {
        return self.spotlight.bannerURL
    }
    
    var description : String {
        return self.spotlight.description
    }
}
