//
//  CashViewModel.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 23/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

struct CashListViewModel {
    let cash: Cash
}

extension CashListViewModel {
    var numberOfSections: Int {
        return 1
    }
    func cashAtIndex(_ index: Int) -> CashViewModel {
        let cashs = self.cash
        return CashViewModel(cashs)
    }
}

struct CashViewModel {
    private let cash: Cash
}

extension CashViewModel {
    init(_ cash: Cash) {
        self.cash = cash
    }
}

extension CashViewModel {
    
    var title: String {
        return self.cash.title ?? ""
    
    }
    
    var bannerURL: String {
        return self.cash.bannerURL ?? ""
    }
    
}

