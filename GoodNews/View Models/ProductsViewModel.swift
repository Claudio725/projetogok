//
//  ProductsViewModel.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 23/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

struct ProductsListViewModel {
    let products: [Products]
}

extension ProductsListViewModel {
    var numberOfSections: Int {
        return 1
    }
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.products.count
    }
    func productsAtIndex(_ index: Int) -> ProductsViewModel {
        let products = self.products[index]
        return ProductsViewModel(products)
    }
}

struct ProductsViewModel {
    private let products: Products
}

extension ProductsViewModel {
    init(_ products: Products) {
        self.products = products
    }
}

extension ProductsViewModel {
    
    var name: String {
        return self.products.name
    
    }
    
    var imageURL: String {
        return self.products.imageURL
    }
    
    var description : String {
        return self.products.description
    }
}

