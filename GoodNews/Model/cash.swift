//
//  cash.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 23/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

struct CashList: Decodable {
    let cash: Cash
}

struct Cash: Decodable {
    let title: String?
    let bannerURL: String?
}
