//
//  Article.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 05/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

struct SpotlightList: Decodable {
    let spotlight: [Spotlight]
}

struct Spotlight: Decodable {
    let name: String
    let bannerURL: String
    let description: String
}


