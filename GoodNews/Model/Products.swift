//
//  Products.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 23/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

struct ProductsList: Decodable {
    let products: [Products]
}

struct Products: Decodable {
    let name: String
    let imageURL: String
    let description: String
}
