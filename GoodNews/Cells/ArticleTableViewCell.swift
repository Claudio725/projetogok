//
//  ArticleTableViewCell.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 06/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation
import UIKit

class ArticleTableViewCell : UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bannerURL: UIImageView!
    @IBOutlet weak var imageURL: UIImageView!
    @IBOutlet weak var imageURL1: UIImageView!
    @IBOutlet weak var imageURL2: UIImageView!
    @IBOutlet weak var digioTitle: UILabel!
    @IBOutlet weak var digioImageURL: UIImageView!
}
