//
//  Webservice.swift
//  GoodNews
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 05/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import Foundation

class Webservice {
    func getArticles(url: URL, completion: @escaping ([Spotlight]?) ->() ) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription) 
                completion(nil)
                
            } else if let data = data {

                let spotlightList = try? JSONDecoder().decode(SpotlightList?.self, from: data)

                if let spotlightList = spotlightList {
                    completion(spotlightList.spotlight)
                }

                print(spotlightList?.spotlight as Any)
                
            }
        }.resume()
    }
    
    func getCash(url: URL, completion: @escaping (Cash?) ->() ) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
                
            } else if let data = data {

                let cashList = try? JSONDecoder().decode(CashList?.self, from: data)

                if let cashList = cashList {
                    completion(cashList.cash)
                }

                print(cashList?.cash as Any)
                
            }
        }.resume()
    }
    
    func getProducts(url: URL, completion: @escaping ([Products]?) ->() ) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
                
            } else if let data = data {

                let productsList = try? JSONDecoder().decode(ProductsList?.self, from: data)

                if let productsList = productsList {
                    completion(productsList.products)
                }

                print(productsList?.products as Any)
                
            }
        }.resume()
    }
}
