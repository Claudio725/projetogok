//
//  CashViewModelTests.swift
//  GoodNewsTests
//
//  Created by CLAUDIO JOSÉ DA SILVA MENEZES on 24/08/20.
//  Copyright © 2020 CLAUDIO JOSÉ DA SILVA MENEZES. All rights reserved.
//

import XCTest
@testable import GoodNews

class CashViewModelTests: XCTestCase {

    private var cashListVM: CashListViewModel!
    
    override func setUp() {
        super.setUp()
        
        //Consome a API e carrega os dados para a view
        let url = URL(string: "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products")!
        
        Webservice().getCash(url: url) { cash in
            if let cash = cash {
                self.cashListVM = CashListViewModel(cash: cash)

            }
        }
        
    }
    
    //Testar se banner do cash está nulo
    func test_bannerIsNotNil() {
        XCTAssertNotNil(self.cashListVM?.cashAtIndex(0).bannerURL)
    }

}
